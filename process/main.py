import os
import sys
import numpy as np

from pathlib import Path

from process import Portrait, ColorBW, ColorRGB


def couleur_moyenne(matrice):
    couleur = np.array([0, 0, 0])
    for i in matrice:
        for j in i:
            couleur += j
    return (1 / (len(matrice) * len(matrice[0]))) * couleur


def distance(p1, p2):
    return ((p1[0] - p2[0]) ** 2 + (p1[1] - p2[1]) ** 2) ** 0.5


def is_in(pixel, partie):
    if pixel[0] > partie[0] and pixel[0] < partie[0] + partie[2]:
        if pixel[1] > partie[1] and pixel[1] < partie[1] + partie[3]:
            return True
    return False


# Initialize tmp folder and img's path
dirpath = Path(os.path.dirname(os.path.realpath(__file__)))
img, tmp = sys.argv[1:3]
tmp = dirpath / tmp

# Create tmp if it doesn't exist
if not os.path.exists(tmp):
    os.makedirs(tmp)

# Empty tmp
for f in os.listdir(tmp):
    file_path = os.path.join(tmp, f)
    try:
        if os.path.isfile(file_path):
            os.unlink(file_path)
    except Exception as e:
        print(e)

portrait = Portrait(img)
x, y, w, h = portrait.get_face()
nose = portrait.get_nose(
    (x + int(w / 4), y + int(h / 4), int(3 * w / 4), int(3 * h / 4))
)
nez = portrait[
    nose[0] + (nose[2] // 2) - 2 : nose[0] + (nose[2] // 2) + 2,
    nose[1] + (nose[3] // 2) - 2 : nose[1] + (nose[3] // 2) + 2,
]


contours = portrait.get_contours()
x, y, w, h = portrait.get_face()
face = portrait.get_face()
eyes = portrait.get_eyes((x, y, w, int(h / 2)))
mouth = portrait.get_mouth((x + int(w / 5), y + int(h / 2), int(4 * w / 5), h))
nose = portrait.get_nose(
    (x + int(w / 4), y + int(h / 4), int(3 * w / 4), int(3 * h / 4))
)

# DESSINER LES YEUX

cptr = -1

if eyes != []:
    for j, contour in enumerate(contours):
        n = len(contour) - 1
        n4 = n // 2
        if (
            is_in(contour[n][0], eyes[0])
            or is_in(contour[n4][0], eyes[0])
            or is_in(contour[n][0], eyes[1])
            or is_in(contour[n4][0], eyes[1])
        ):
            cptr += 1
            for i, ctr in enumerate(
                np.array_split(contour, max(len(contour) // 50, 3))
            ):
                portrait.draw_contours([ctr])
                portrait.save(f"{tmp}/{cptr}_{i}.png")


# DESSINER LE NEZ

for j, contour in enumerate(contours):
    n = len(contour) - 1
    n1 = n // 2
    if is_in(contour[n][0], nose) or is_in(contour[n1][0], nose):
        cptr += 1
        for i, ctr in enumerate(np.array_split(contour, max(len(contour) // 50, 3))):
            portrait.draw_contours([ctr])
            portrait.save(f"{tmp}/{cptr}_{i}.png")


# DESSINER LA BOUCHE
for j, contour in enumerate(contours):
    n = len(contour) - 1
    n2 = n // 2
    if is_in(contour[n][0], mouth) or is_in(contour[n2][0], mouth):
        # si ressemble précédent : continue
        cptr += 1
        for i, ctr in enumerate(np.array_split(contour, max(len(contour) // 50, 5))):
            portrait.draw_contours([ctr])
            portrait.save(f"{tmp}/{cptr}_{i}.png")


# DESSINER LA FACE

cptr += 1
old = np.copy(portrait.canvas)
for j, contour in enumerate(contours):
    n = len(contour) - 1
    n3 = n // 2
    if is_in(contour[n][0], face) or is_in(contour[n3][0], face):
        if (
            not (is_in(contour[n][0], mouth))
            and not (is_in(contour[n2][0], mouth))
            and not (is_in(contour[n][0], nose))
            and not (is_in(contour[n1][0], nose))
        ):
            drawn = False
            k = 0
            for i, ctr in enumerate(
                np.array_split(contour, max(len(contour) // 50, 8))
            ):
                portrait.draw_contours([ctr])
                if np.linalg.norm(portrait.canvas - old) >= 20:
                    drawn = True
                    old = np.copy(portrait.canvas)
                    portrait.save(f"{tmp}/{cptr}_{k}.png")
                    k += 1
            if drawn:
                cptr += 1

# DESSINER LE RESTE

old = np.copy(portrait.canvas)
for j, contour in enumerate(contours):
    if (
        not (is_in(contour[n][0], mouth))
        and not (is_in(contour[n2][0], mouth))
        and not (is_in(contour[n][0], nose))
        and not (is_in(contour[n1][0], nose))
        and not (is_in(contour[n][0], face))
        and not (is_in(contour[n3][0], face))
    ):
        drawn = False
        k = 0
        for i, ctr in enumerate(np.array_split(contour, max(len(contour) // 50, 8))):
            portrait.draw_contours([ctr])
            if np.linalg.norm(portrait.canvas - old) >= 30:
                drawn = True
                old = np.copy(portrait.canvas)
                portrait.save(f"{tmp}/{cptr}_{k}.png")
                k += 1
        if drawn:
            cptr += 1

portrait = Portrait(img, bg=True)
portrait.save(f"{tmp}/{cptr}_{k}.png")
