import numpy as np
import os
import cv2

from pathlib import Path


haar_path = Path(os.path.dirname(os.path.realpath(__file__))) / "haar"


class ColorRGB:
    """Represent an RGB color from a tuple (r,g,b)."""

    def __init__(self, rgb, tolerance=10):
        self.rgb = rgb
        self.tolerance = tolerance
        self._hue = cv2.cvtColor(np.uint8([[rgb]]), cv2.COLOR_BGR2HSV)[0][0][0]

    @property
    def lower(self):
        return np.array([self._hue - self.tolerance, 30, 30])

    @property
    def upper(self):
        return np.array([self._hue + self.tolerance, 255, 255])


class ColorBW:
    """Represent a grayscale range of colors."""

    def __init__(self, lower, upper):
        self.lower = lower
        self._upper = upper

    @property
    def upper(self):
        return min(255, self._upper)


class Portrait(np.ndarray):
    """Subclass of numpy.ndarray which represents an image."""

    def __new__(cls, path, height=700, bg=False):
        img = cv2.imread(path).view(cls)
        scale = height / img.shape[0]
        width = int(scale * img.shape[1])
        portrait = cv2.resize(img, (width, height)).view(cls)
        portrait.gray = cv2.cvtColor(portrait, cv2.COLOR_BGR2GRAY)
        if bg:
            portrait.canvas = np.copy(portrait.gray)
        else:
            portrait.canvas = np.ones(portrait.shape, np.uint8) * 255
        return portrait

    def show(self, name=""):
        """Display the frame."""
        cv2.imshow(name, self.canvas)
        k = cv2.waitKey(0)
        if k == 27:  # wait for ESC key to exit
            cv2.destroyAllWindows()

    def save(self, name="portrait.png"):
        """Save the frame."""
        cv2.imwrite(name, self.canvas)

    def get_centroid(self, color):
        """Return the centroid of the biggest shape of the given color."""
        hsv = cv2.cvtColor(self, cv2.COLOR_BGR2HSV)
        mask = cv2.inRange(hsv, color.lower, color.upper)
        contours = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[1]
        if not contours:
            return None
        ctr = max(contours, key=len)
        moments = cv2.moments(ctr)
        if not moments["m00"] or len(ctr) < 10:
            return None
        center_x = int(moments["m10"] / moments["m00"])
        center_y = int(moments["m01"] / moments["m00"])
        return center_x, center_y

    def draw_centroid(self, color):
        """Draw the centroid of the biggest shape of the given color."""
        if self.get_centroid(color) is not None:
            cv2.circle(self.canvas, self.get_centroid(color), 5, (0, 255, 255), -1)

    def get_contours(self, nb=None):
        """Return the contours of the shapes whose colors are within the given
    range"""
        img = cv2.Canny(self, 100, 200)
        contours = cv2.findContours(img, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)[1]
        if not contours:
            return None
        contours.sort(key=len, reverse=True)
        contours = list(filter(lambda l: len(l) > 30, contours))
        return contours[:nb]

    def show_shadow(self):
        cv2.imshow("", cv2.Laplacian(self, cv2.CV_8U, ksize=3))
        k = cv2.waitKey(0)
        if k == 27:  # wait for ESC key to exit
            cv2.destroyAllWindows()

    def draw_contours(self, contours, color=(0, 0, 0)):
        if not contours:
            return
        for ctr in contours:
            cv2.drawContours(self.canvas, ctr, -1, color, 2)

    def write_text(self, text, coor, size):
        cv2.putText(
            self.canvas,
            text,
            coor,
            cv2.FONT_HERSHEY_SIMPLEX,
            size,
            (0, 255, 0),
            2,
            cv2.LINE_AA,
        )

    def draw_rect(self, rect, color=(0, 0, 0)):
        if rect is None:
            return
        x, y, w, h = rect
        cv2.rectangle(self.canvas, (x, y), (x + w, y + h), color, 2)

    def get_face(self, rect=(None,) * 4):
        face_cascade = cv2.CascadeClassifier(str(haar_path / "frontal_face.xml"))
        faces = face_cascade.detectMultiScale(self.gray, 1.05, 5)
        face = max(faces, key=self.__size_rect)
        return face

    def get_eyes(self, rect=(None,) * 4):
        x, y, w, h = rect
        eye_cascade = cv2.CascadeClassifier(str(haar_path / "eye.xml"))
        sub = self.gray[y : y + h, x : x + w]
        eyes = eye_cascade.detectMultiScale(sub, 1.05, 5)
        eyes = sorted(eyes, key=self.__size_rect)
        for eye in eyes:
            eye += np.array([x, y, 0, 0])
        return eyes[: max(2, len(eyes))]

    def get_mouth(self, rect=(None,) * 4):
        x, y, w, h = rect
        eye_cascade = cv2.CascadeClassifier(str(haar_path / "mouth.xml"))
        sub = self.gray[y : y + h, x : x + w]
        mouths = eye_cascade.detectMultiScale(sub, 1.05, 5)
        if mouths == ():
            return None
        mouth = max(mouths, key=self.__size_rect)
        return mouth + np.array([x, y, 0, 0])

    def get_nose(self, rect=(None,) * 4):
        x, y, w, h = rect
        eye_cascade = cv2.CascadeClassifier(str(haar_path / "nose.xml"))
        sub = self.gray[y : y + h, x : x + w]
        noses = eye_cascade.detectMultiScale(sub, 1.05, 5)
        if noses == ():
            return None
        nose = max(noses, key=self.__size_rect)
        return nose + np.array([x, y, 0, 0])

    @staticmethod
    def __size_rect(rect):
        return rect[2] + rect[3]
