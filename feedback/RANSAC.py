﻿import cv2
import numpy as np
import math
import random
import matplotlib.pyplot as plt
import Homography

def equation(point1,point2):
    x1,y1 = point1
    x2,y2 = point2

    u = x2-x1
    v = y2-y1

    a = -v
    b = u
    c = v*x1-u*y1

    return a,b,c

#Donne l'équation cartésienne de la droite définie par les quatre coordonnées passées en entrée
#(On represente une droite par deux points (deux tuples), ou par un tuple (a,b,c) correspondant a l'equation de la droite ax+by+c

def distance_droite(point,point1,point2):
	x,y = point
	a,b,c = equation(point1,point2)
	return abs(a*x+b*y+c)/math.sqrt(a**2+b**2)

#Renvoie la distance d'un point à une droite

def getContours(sourceImage):

	im = cv2.imread(sourceImage)

	imgray = cv2.cvtColor(im,cv2.COLOR_BGR2GRAY)

	ret,thresh = cv2.threshold(imgray,127,255,0)
	im2, contours, hierarchy = cv2.findContours(thresh,cv2.RETR_LIST,cv2.CHAIN_APPROX_NONE)

	return contours

#Renvoie la liste des contours de sourceImage (Liste de listes de points)

def intersection(point1,point2,point3,point4) :

	#line1 est de la forme

	a1, b1, c1 = equation(point1,point2)
	a2, b2, c2 = equation(point3,point4)

	if abs(b1) < 0.01 :
		return (round(-c1/a1), round(-c2/b2))

	if abs(b2) < 0.01 :
		return (round(-c2/a2), round(-c1/b1))

	b1_2 = -a1/b1
	c1_2 = -c1/b1

	b2_2 = -a2/b2
	c2_2 = -c2/b2

	x = (c2_2 - c1_2) / (b1_2 - b2_2)
	y = b1_2*x + c1_2

	return (round(x), round(y))

#Renvoie les coordonnées du point d'intersection de deux droites (définies par les 8 coordonnées passées en entrée)

def horizontal(point1,point2):
	a,b,_ = equation(point1,point2)
	return abs(a) < 0.1*abs(b)

def vertical(point1,point2):
	a,b,_ = equation(point1,point2)
	return abs(b) < 0.1*abs(a)

#Permet de savoir dans l'algorithme de Ransac si la droite considérée est horizontale ou non.

def supprimer_doublons(liste):
	l = len(liste)//2
	for i in range(l):
		for j in range(l):
			if i != j and (abs(liste[i][0]- liste[j][0]) < 1 and abs(liste[i][1]-liste[j][1]) < 1 ):
				del(liste[j])
				print("Suppr")
	return liste


##############################Algorithme de Ransac permettant d'obtenir les quatre coins de la feuille

def ransac(sourceImage,seuil):

	contours_list = getContours(sourceImage)
	contour_sizes = [(cv2.contourArea(contour), contour) for contour in contours_list]
	biggest_contour = max(contour_sizes, key=lambda x: x[0])[1]

	n = np.size(biggest_contour)

	points_votants = []
	contours = []

	horizontal_lines = []
	vertical_lines = []

	img = cv2.imread(sourceImage)
	width, height = img.shape[:2]
	img = cv2.resize(img, (width//10,height//10), interpolation =cv2.INTER_AREA)


	for i in range(n//2):
		points_votants.append([biggest_contour[i][0][0],biggest_contour[i][0][1],[]])

	#La liste points_votants est de la forme [...,[point, [liste des droites pour lesquelles il a voté]],...]

	for _ in range (4):
		contours.append(nouvel_echantillon(points_votants,seuil,height//10))
		#L'appel à nouvel_echantillon renvoie la droite qui a entraîné le plus de vote et supprime de la liste
		#points_votants les points situés près de cette droite

	while bon_resultat(contours) == False:
		ransac(sourceImage,seuil)

	for line in contours:
		x1,y1,x2,y2 = line
		if horizontal((x1,y1),(x2,y2)):
			horizontal_lines.append(line)
		else:
			vertical_lines.append(line)

	#print(vertical_lines)
	#print(horizontal_lines)

	xA,yA,xB,yB = horizontal_lines[0]
	xC,yC,xD,yD = vertical_lines[0]
	xE,yE,xF,yF = horizontal_lines[1]
	xG,yG,xH,yH = vertical_lines[1]

	a = 	[intersection((xA,yA),(xB,yB),(xC,yC),(xD,yD)),
			 intersection((xA,yA),(xB,yB),(xG,yG),(xH,yH)),
			 intersection((xE,yE),(xF,yF),(xC,yC),(xD,yD)),
			 intersection((xE,yE),(xF,yF),(xG,yG),(xH,yH))]
			#a est la liste des coins de la feuille

	x1, x2,y1, y2= horizontal_lines[0]
	x3, x4,y3, y4= horizontal_lines[1]
	x5, x6,y5, y6= vertical_lines[0]
	x7, x8,y7, y8= vertical_lines[1]

	"""plt.plot([x1, y1], [x2, y2], color='b', linestyle='-', linewidth=2)
	plt.plot([x3, y3], [x4, y4], color='b', linestyle='-', linewidth=2)
	plt.plot([x5, y5], [x6, y6], color='b', linestyle='-', linewidth=2)
	plt.plot([x7, y7], [x8, y8], color='b', linestyle='-', linewidth=2)
	plt.plot(*zip(*a), marker='o', color='r', ls='')

	"""
	img = cv2.imread(sourceImage)
	"""cv2.drawContours(img, biggest_contour, -1, (0,255,0), 3)
	cv2.namedWindow("image",cv2.WINDOW_NORMAL)
	cv2.resizeWindow("image",600,600)
	cv2.imshow("image",Homography.four_point_transform(img, a))
	cv2.waitKey(0)
	cv2.destroyAllWindows()"""

	return Homography.four_point_transform(img, a)

def bon_resultat(l):
	c_h = 0
	c_v = 0
	for line in l:
		x1,y1,x2,y2 = line[0],line[1],line[2],line[3]
		point1 = (x1,y1)
		point2 = (x2,y2)
		if horizontal(point1,point2):
			c_h += 1
		elif vertical(point1,point2):
			c_v += 1
	return c_h == 2 and c_v == 2





def nouvel_echantillon(points_votants,seuil,echant):

	n = len(points_votants)//2
	liste_droite_score = []

	for _ in range(n//echant):

		i = random.randint(0,n)
		x1,y1,_ = points_votants[i]
		i = random.randint(0,n)
		x2,y2,_ = points_votants[i]
		#On choisit deux points au hasard parmi les points du contours (i.e. les points de points_votants)

		if (x1,y1) != (x2,y2):

			point1,point2 = (x1,y1),(x2,y2)
			score = 0

			for j in range(n):
				x,y,_ = points_votants[j]
				if distance_droite((x,y),point1,point2) < seuil:
					score += 1
					points_votants[j][2].append([x1,y1,x2,y2])

				liste_droite_score.append([[x1,y1,x2,y2],score])


	liste_droite_score = sorted(liste_droite_score, key = lambda liste_droite_score :liste_droite_score[1],reverse = True )
	#On trie la liste des droites affectées de leur score par ordre de score décroissant

	l = liste_droite_score[0][0]

	"""print("Droite:",l)
	print("Score:",liste_droite_score[0][1])"""

	for i in range(n,0,-1):
		x,y = points_votants[i][0],points_votants[i][1]
		x1,y1,x2,y2 = l
		if distance_droite((x,y),(x1,y1),(x2,y2)) < seuil:
			del(points_votants[i])

	return l

"""
Tests à effectuer sur les images
angle.jpg ; angle_bloc.jpg ; bloc.jpg ; ExempleFeuille.jpg ; ExempleFeuille2.jpg ; imref.jpg ; loin.jpg ; normale.jpg
du répertoire Python
"""


"""
print(l)
"""
