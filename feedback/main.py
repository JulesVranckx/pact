import RANSAC, Homography, compare, Dist_cnt
import json
import cv2
import sys
import numpy as np
import matplotlib.pyplot as plt
import math
from RANSAC import ransac

def pre_traitement(img_ref_path, img_usr_path):
	#On enregistre l'image de reference et l'image de l'utilisateur,traitees, dans le repertoire courant
	img_ref = cv2.imwrite('./img_ref.jpg', cv2.imread(img_ref_path))
	#plt.imshow(cv2.imread('./img_ref.jpg'))
	#plt.show()
	height, width,_ = (cv2.imread('./img_ref.jpg')).shape
	img_usr = cv2.imwrite('./img_usr.jpg',ransac(img_usr_path,10))


#On charge les deux images via ligne de commande + on recadre l'image de l'utilisateur
path1 = sys.argv[1]
path2 = sys.argv[2]
json1 = sys.argv[3]

pre_traitement(path1,path2)

#On calcule la note de l'utilisateur qu'on stocke dans datas
note = Dist_cnt.note("./img_usr.jpg","./img_ref.jpg")
datas = {"score" : note}
note = max(0,round(note,1))
data_twt = str(note)

with open(json1, 'w') as f:
    f.write(data_twt)

#json.dumps(datas, indent=4)
