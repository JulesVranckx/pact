import cv2
from math import sqrt
import numpy as np


SEUIL1 = 80
SEUIL2 = 150
NBR_CONTOURS = 5

def depart(cnt):
	"""Renvoie le depart du contour"""
	return cnt[0][0]

def arrivee(cnt):
	"""Renvoie le point de fin du contour"""
	return cnt[-1][0]

def milieu(cnt):
	"""Renvoie le milieu du contour"""
	n = len(cnt)
	return cnt[n//2][0]

def pot_same(cnt1, cnt2, seuil):
	"""Renvoie un booeen indiquant si les contours sont potentiellement les memes"""
	dep1 = depart(cnt1)
	dep2 = depart(cnt2)
	end1 = arrivee(cnt1)
	end2 = arrivee(cnt2)
	milieu1 = milieu(cnt1)
	milieu2 = milieu(cnt2)

	if dist_pts(dep1, dep2) < seuil and dist_pts(end1,end2) < seuil and dist_pts(milieu1,milieu2):
		#print("Pris")
		return True
	return False


def dist_pts(p1,p2):
	""" Distance euclidienne entre deux points"""
	[x1, y1] = p1
	[x2, y2] = p2

	d = sqrt((x2 - x1)**2 + (y2 - y1)**2)

	return d

def dist_point_plus_proche(pt, i, cnt,seuil):
	"""Point le plus proche de pt dans cnt"""
	minimun = -1
	n = len(cnt)

	deb = max(0,i-seuil)
	fin = min(n,i+seuil)

	for j in range(deb,n):
		d = dist_pts(pt[0], cnt[j][0])
		if d < minimun or minimun < 0 :
			minimun = d
	#print("Point min = "+ str(minimun))
	return minimun

def dist_cnt(cnt1,cnt2):
	d = 0

	for i in range(len(cnt1)):
		d += dist_point_plus_proche(cnt1[i],i,cnt2,SEUIL2)
	#print(d / len(cnt1))
	return d / len(cnt1)

def contour_plus_proche(cnt, cnt_list):
	"""Distance au contour le plus proche"""
	min = -1
	ret = cnt_list[0]
	for contour in cnt_list:
		if pot_same(cnt, contour, SEUIL1):
			d = dist_cnt(cnt, contour)
			if d < min or min < 0:
				min = d
				ret = contour
	return ret

def note(path1, path2):

	im1 = cv2.imread(path1,0)
	im2 = cv2.imread(path2,0)
	im1 = cv2.resize(im1, (500,354))
	im2 = cv2.resize(im2, (500,354))
	ret1, thresh1 = cv2.threshold(im1,100,255,0)
	ret2, thresh2 = cv2.threshold(im2,100,255,0)


	#cv2.imshow("",thresh1)
	#cv2.waitKey()
	#cv2.destroyAllWindows()

	_,contours1, hierarchy1 = cv2.findContours(thresh1,1,2)
	_,contours2, hierarchy2 = cv2.findContours(thresh2,1,2)

	"""contours1 = sorted(contours1, key = len, reverse = True)[:NBR_CONTOURS]
	contours2 = sorted(contours2, key = len, reverse = True)[:NBR_CONTOURS]"""

	d = 0
	matchshape_max = 0
	MIN = min(len(contours1),len(contours2))
	note = 0
	for i in range(MIN):
		if i < len(contours1):
			ms = cv2.matchShapes(contours1[i],contour_plus_proche(contours1[i],contours2),1,0.0)
			d = d + ms
			if ms > matchshape_max :
				matchshape_max = d

		#print("d :"+ str(d))
		#print("Coeff" + str(d/MIN))
	#print("Resultat final",d/MIN)
	return (((((matchshape_max-(d/MIN)) / matchshape_max) * 20) - 16 ) *5)
