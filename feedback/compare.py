from __future__ import division
from math import log, sqrt
import cv2
import numpy

#Methode SSIM deja implementee
from skimage.measure import compare_ssim

# Implementation de la methode MSE, comparaison quadratique pixel a pixel.
def MSE(img1_path, img2_path) :
	"""Simple comparaison pixel a pixel de l'image"""
	img1 = cv2.imread(img1_path,0)
	img2 = cv2.imread(img2_path,0)
	img1 = cv2.resize(img1,(500,354))
	img2 = cv2.resize(img2,(500,354))
	n = 500; m = 354
	EQM = 0
	dim = n*m

	for i in range(n):
		for j in range(m):
			EQM += (img1[i][j] - img2[i][j])**2

	return 1 -(EQM / (dim * 255**2))

# Pour utiliser la fonction SSIM avec un chemin
def SSIM(img1_path, img2_path):

	img1 = cv2.imread(img1_path,0)
	img2 = cv2.imread(img2_path,0)
	img1 = cv2.resize(img1,(500,354))
	img2 = cv2.resize(img2,(500,354))
	img1 = cv2.threshold(img1,100,255,0)
	img2 = cv2.threshold(img2,100,255,0)
	return compare_ssim(img1,img2, full = True)

# Detection des zones d'erreurs
def diff(img1, img2):
	n,m = img1.shape
	ret = numpy.eye(n,m)
	for i in range(n):
		for j in range(m):
			ret[i][j] = 1 - abs(img1[i][j] - img2[i][j])
	return ret

def find_feature_zone(img1_path, img2_path):
	#Ouverture des liste_images
	img1 = cv2.imread(img1_path,0)
	img2 = cv2.imread(img2_path,0)
	img1 = cv2.resize(img1,(500,354))
	img2 = cv2.resize(img2,(500,354))
	ret,thresh1 = cv2.threshold(img1,100,255,0)
	ret,thresh2 = cv2.threshold(img2,100,255,0)
	difference = diff(thresh1,thresh2)
	cv2.imshow("test", thresh1)
	cv2.waitKey(0)
	cv2.destroyAllWindows()
	cv2.imshow("test", thresh2)
	cv2.waitKey(0)
	cv2.destroyAllWindows()
	cv2.imshow("test", difference)
	cv2.waitKey(0)
	cv2.destroyAllWindows()
	_,contours,hierarchy= cv2.findContours(thresh1, 1, 2)
	cnt = contours[0]
	rect = cv2.minAreaRect(cnt)
	box = cv2.boxPoints(rect)
	box = numpy.int0(box)
	thresh1 = cv2.drawContours(thresh1,contours,0,(0,0,255),2)
	cv2.imshow("test", thresh1)
	cv2.waitKey()
	cv2.destroyAllWindows()


	return None
