=== Schéma d’architecture

image::../images/SchemaPACT-1.png[Schéma d'architecture,800]

==== Description des blocs

==== Traitement d’image : 
L’utilisateur charge sa photo (portrait de face, sur fond clair 
et uni) dans le logiciel. Celle-ci est alors traitée par un algorithme 
qui en extrait les contours, détermine ceux devant être gardés ou non, 
et les regroupe en « étapes ». La façon dont sont sélectionnés et triés
les traits dépend de techniques de dessin. 

==== Feedback / Correction :
A partir de la photo prise par la caméra, un autre algorithme de 
traitement d’image « mesure la réussite » du dessin. Après avoir
bien recadré la photo, elle est comparée à celle téléchargée initialement. 
Par une mesure de distances, de rayons de courbures, etc sur la superposition des
deux images, l’algorithme détermine des zones à améliorer et une note globale du dessin. 

==== Interface Utilisateur Graphique :
C’est la fenêtre qui s’affiche sur l’ordinateur de l’utilisateur.
Il y retrouve un menu pour importer ses photos ou en sélectionner des 
pré-enregistrées, retrouver des photos de ses anciens dessins. Pendant
le dessin, Il y voit à la fois la photo originale et les traits projetés, 
le numéro de l’étape. Lors de la phase d’évaluation (feedback), les zones 
de forte dissemblance sont entourées en rouge. Des fenêtres pop-up s’ouvrent
pour donner des conseils au dessinateur. 

==== Projecteur + Caméra : 
Ils créeront une seule et même entité physique, sous la forme d’une table
dans laquelle sera placé le projecteur, avec pour plateau une vitre sur 
laquelle l’utilisateur posera sa feuille à dessin. Le tout sera relié à une 
webcam fixe qui prendra donc la table en photo à la fin de chaque étape.