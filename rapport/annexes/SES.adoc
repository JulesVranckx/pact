== Module SES 

L'enjeu de ce module a été pour nous de rendre notre projet utilisable par tous
les passionnés de dessin, tant les amateurs que les professionnels. 

Une de nos premières lignes directrices a été de contacter des experts en dessin, 
afin de connaitre en détail les techniques d'ébauche d'un portrait. Déterminer 
l'ordre des traits, porter un jugement sur la ressemblance entre un dessin et un
visage, lier reconnaissance faciale et création d'un tutoriel, voici quelques exemples
de questions auxquelles des interviews et des rencontres avec des personnes expérimentées
ont permis de répondre.

Nous avons demandé à 4 différentes personnes de dessiner les mêmes portraits. En étudiants
les similarités et dissemblances des techniques entre les experts (cf. PFD fourni en annexes), 
il en ressort notamment que l'ordre dans lequel sont dessinés les traits est arbitraire 
et assez variable d'un dessinateur à un autre. 
Déterminer l'ordre dans lequel les traits sont dessinés a donc été notre premier enjeu.
Nous avons décidé,pour satisfaire une majorité d'utilisateurs, d'afficher les traits 
dans un ordre correspondant aux principales caractéristiques d'un visage : d'abord le nez, 
puis les yeux, puis la bouche, etc. Cette prise de décision a eu de grosses conséquences
sur la manière dont sont implémentés les modules Interface Graphique et Détection de Contours.

Ensuite, pour le module FeedBack, nous avons du décider des critères permettant de juger 
la réussite et la similarité d'un dessin avec un visage donné en photo. 
L'éloignement entre les traits n'est pas suffisant pour dire si un dessin est raté, et les
variations rapides des traits n'est pas non plus adéquat pour juger de la réussite d'un dessin.
Ainsi, c'est en faisant une appréciation globale sur le dessin, grâce à une comparaison
traits par traits que l'algorithme peut juger de la réussite d'un dessin, et propose une
note à l'utilisateur, pour l'encourager à améliorer son oeuvre. 

Par ailleurs, la table a été matière de recherche et  a été soumise à des tests utilisateurs. En effet, nous 
avions proposé une première ébauche de la table à dessin. Mais après nos prises de contact avec 
les dessinateurs expérimentés, ceux-ci nous ont suggéré d'améliorer le confort de la table, notamment 
en proposant au dessinateur d'être assis, et d'augmenter la place disponible pour ses jambes. 
Pour rentrer à la fois dans le budget, et prendre en compte les remarques des utilisateurs, nous 
avons dû ébaucher une autre table qui était plus convenante. Cependant, nous aurions aimé 
pouvoir incliner le support de travail de la table pour améliorer encore le confort, mais par 
contrainte de matériel et de budget, nous avons été restreints. 

Pour finir, nous avons fait tester notre projet à deux des quatres personnes précédemment rencontrées. 
La rapidité de l'affichage dynamique, et le nombre d'images par étape a été changé, et adapté aux 
convenances des utilisateurs.

==== Bibliographie - Contact de nos interviewés

Anne-Sophie BUI KHAC : Graphiste Illustrateur Freelance, as.buikhac@gmail.com, contacté via Linkedin

Antoine DUBOIS : Etudiantà l'EPMC La Ruche, contacté via Messenger

Emmanuel CHOULIER : Etudiant à Telecom ParisTech, contacté via Messenger

Pierre-Louis CORDONNIER : Etudiant à ENS Cachan, pl.cordonnier@gmail.com,  contacté via Messenger
