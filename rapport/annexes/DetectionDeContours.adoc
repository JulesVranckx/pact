== Détection de contours



Notre module se propose d’extraire les contours qui nous intéressent en vue de la réalisation du dessin mais aussi de gérer leur taille, et l’ordre dans lequel ils apparaissent, en lien avec les études du module SES.


=== Détecter les contours


Nous avons essayé divers types de filtres à appliquer successivement sur les photos, mais nous avons finalement opté pour le fitre de Canny. Celui-ci commence par appliquer un filtre gaussien en vue de lisser l’image. Ensuite, afin d’effectuer des comparaisons entre pixels voisins, il réalise des calculs concernant la norme et la normale au gradient de chaque pixel de l’image filtrée. Il s’agit ensuite de regarder les maximas de gradient pour déterminer si un pixel est un contour ou non, et de définir deux seuils. Le premier seuil est le seuil haut : tout les pixels supérieurs au seuil haut sont conservés. De même, tout ceux inférieurs au seuil bas sont conservés. Et enfin, pour les pixels situés entre les deux seuils, ils sont conservés si ils sont en contact directs avec un pixel déjà défini comme contour.

=== Ordonner les contours


Pour ce qui est de l’ordre des contours, il nous fallait déjà être capable de distinguer les éléments clés du visage : les yeux, le nez et la bouche. C’est ce qu’on permis les cascades de haar, fournies dans la bibliothèque OpenCV, qui s’appuie sur des classifieurs comportant des milliers d’exemplaires de yeux, de bouche et de nez. En lien avec le module SES, nous avons sélectionné tout d’abord les contours présents dans le nez, puis ceux de la bouche et des yeux, avant de tracer les autres contours.
