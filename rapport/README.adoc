= Rapport d’avancement du groupe “3.2”
PACT groupe 3.2 (SYMPACT)
ifdef::env-gitlab,env-browser[:outfilesuffix: .adoc]
:doctype: book
:stem: latexmath
:source-highlighter: coderay
:toc-title: Table des matières
:toc: macro
:sectnums:
:imagesdir: images


// Partie non numérotée
:sectnums!:
== LES SYMPACT
=== Membres du groupe

* Pablo Pevsner
* Maha Meihemid
* Paul Fayard
* Théo Daneels
* Jules Vranckx
* Aurore Gosmant

=== Tuteur

* Michel Roux

=== Encadrant génie logiciel

* Jean-Claude Moissinac

== Résumé du sujet choisi en français (PAN1)

Notre projet doit permettre à l’utilisateur de dessiner un visage qu’il a 
pris en photo, trait par trait. On impose à la photo d’être prise de face, sur 
fond clair et uni. L’image est tout d’abord chargée sur l’ordinateur puis 
traitée par le logiciel qui génère une suite d’instructions permettant de la 
dessiner de manière réaliste, étape par étape. 
L’utilisateur se sert pour cela d’un dispositif composé d’une plaque de verre 
sur laquelle le projecteur affiche les instructions, sous forme de traits 
lumineux s’affichant progressivement : en posant une feuille de papier sur la 
plaque, il peut dessiner par dessus les traits, par transparence. 
Une caméra, installée au dessus de ce dispositif, permet également de réaliser 
un feedback. En effet, elle prend des photos de la feuille à la fin de chaque 
étape pour les transmettre au logiciel qui se chargera de suggérer à 
l’utilisateur des amélioration possibles.

Ce projet étant centré sur le dessin, il s’inscrit dans le thème de l’Art;
le traitement d’image, l’optique et la conception logicielle sont quant à eux les 
aspects scientifiques du projet.

== English Summary (PAN1)


Our project will allow users to draw portraits from pictures of faces they 
took themselves. The photos must be of face, with a plain and light background. 
First, the image is downloaded on the computer. It is then processed by the 
software, generating a sequence of instructions which will be used to describe 
the different steps of the drawing. In order to draw the picture, the user has 
to put a sheet of paper on a device composed of a glass plate, where a projector 
displays the instructions in the form of light lines which appear progressively. 
The user can follow the instructions by transparency.

A camera is installed above the device in order to give a feedback: it takes 
pictures of the sheet of paper at the end of each step and transfers it to the 
software, which will take care of suggesting possible improvements to the user.
As this project focuses on drawing, it falls within the subject Art; image 
processing, optic and software design are on the other hand the scientific part 
of the project.  



// en PDF on ne peut pas controler la position de la toc
// Macros non supportées
ifndef::backend-pdf[]
== Table des matières
:toc-title:
toc::[]
endif::[]

// On numérote le reste des sections
:sectnums:

== Étude d’antériorité et justification de la proposition 

include::proposition/proposition.adoc[Description de la proposition]

include::proposition/etat-de-l-art.adoc[Description de l’état de l’art]

== Scénarios d’usage

include::scenario/scenario.adoc[Scénarios d’usage]

<<<

== Architecture du projet 

include::architecture/schema.adoc[Schéma d’architecture]

include::architecture/interfaces.adoc[Description des interfaces]

//include::architecture/sequence.adoc[Diagramme de séquence]

include::architecture/ihm.adoc[Interface utilisateur graphique]

include::architecture/taches.adoc[Tableau détaillé des tâches]

<<<

== Organisation du projet 

include::organisation/planification.adoc[Diagramme de planification temporel des tâches]

include::organisation/repartition.adoc[Répartition des élèves par module]

//include::organisation/plan-tests.adoc[Plans de test (PAN2+)]

//include::organisation/avancement.adoc[Diagramme d’avancement des tâches (PAN2+)]

<<<

include::annexes/Erreur&Feedback.adoc[Erreur&Feedback]

include::annexes/DetectionDeContours.adoc[Detection de contours]

include::annexes/InterfaceGraphique.adoc[Interface graphique]

include::annexes/SES.adoc[SES]

include::annexes/TableADessin.adoc[Table à dessin]

////

[bibliography]
== Bibliographie (PAN1+)

include::References.adoc[Bibliographie]

<<<

== Annexes

include::annexes/modifications.adoc[Modifications (PAN2+)]

include::annexes/reunions.adoc[Comptes Rendus de réunions]

include::annexes/avancement.adoc[Avancement des modules]

include::annexes/moduleX.adoc[Avancement module X]

include::annexes/moduleY.adoc[Avancement module Y]
