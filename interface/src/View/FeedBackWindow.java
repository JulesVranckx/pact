package View;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class FeedBackWindow extends JFrame {
	private final Window window;
	private double score=0;
	
	public FeedBackWindow(Window window,String file) {
		super("FeedBack");
		this.window = window;
         String line="";  
		  
		FileReader fr = null;
		  BufferedReader br = null;
		  try { fr = new FileReader(file);
		  br = new BufferedReader(fr); 
		  line=br.readLine();
		  } catch (IOException e1) {
		  e1.printStackTrace();
		  }
		JPanel jp=new JPanel();
		  JLabel title = new JLabel(
					"<HTML><strong>*****Voici la note obtenue pour votre dessin</strong>****<br>"
				  
				  + "<br><HTML>"+"\n\n");
		  JLabel lines = new JLabel(line);
		  title.setForeground(Color.BLACK);
		  lines.setFont(new Font("Serif", Font.ITALIC, 48));
		  lines.setForeground(new Color(4,204,223));
		  title.setFont(new Font("Serif", Font.BOLD, 32));
		  jp.add(title);
		  jp.add(lines);
		  jp.setBackground(new Color(230, 230, 230));
		  this.setContentPane(jp);
		  //this.setLocationRelativeTo(null);
		  Toolkit toolkit = getToolkit();

		  Dimension size = toolkit.getScreenSize();
		  
		  this.pack(); //sizes automatically the window
		  this.setLocation(size.width/2 - getWidth()/2, size.height/2 - getHeight()/2);
	      setVisible(true) ;
	}

	
	

	}

