package View;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.ImageObserver;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import Controller.ImageUtils;

public class ProjectionPanel extends JPanel {
	private ArrayList<ImageIcon> traitList;
	private final Window window;
	private int k;
	private int etape;

	public ProjectionPanel(Window window) {
		super();
		this.window = window;
		//this.setLayout(new GridLayout(10,10));
		etape = window.getModel().getEtape();
		this.traitList = window.getModel().getTraitList().get(etape);
		this.setBackground(Color.WHITE);


	}

	public void notifyForUpdate() {
		repaint();
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		etape = window.getModel().getEtape();
		k = window.getModel().getCount();
		this.traitList = window.getModel().getTraitList().get(etape);

		ImageIcon icon = new ImageIcon(traitList.get(k).getImage().getScaledInstance(this.getWidth() - 10, this.getHeight() - 10, Image.SCALE_DEFAULT));
		Image image = icon.getImage();
		Image ro = ImageUtils.rotateImage(image, 90);
		Image h = ro.getScaledInstance(this.getWidth(), this.getHeight(), Image.SCALE_DEFAULT);
		if(!window.getModel().isIsmodified())
			g.drawImage(h, 0, 0, this);
		if(window.getModel().isIsmodified()) {
			ImageIcon noir=
					new ImageIcon((new ImageIcon("../../img/fondNoir.png").getImage().getScaledInstance(this.getWidth()-10, this.getHeight()-10, Image.SCALE_DEFAULT)));

			g.drawImage(noir.getImage(), 0, 0, this);
		}
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (k >= 1) {
			showThree(k, 0);
			if (diff(k % 3, 0) == false) {
				if (k > 1) {
					window.getModel().setCount(k - 1);
				}
			}
		}
	}

	public boolean diff(int num, int i) {
		if (i > num) {
			int c = i;
			i = num;
			num = c;
		}
		if (num - i == 2) {
			return false;
		}
		return true;
	}

	public void showThree(int arg1, int j) {
		if ((arg1 < traitList.size() - 1) && (diff(arg1 % 3, j) == true)) {
			window.getModel().setCount(arg1 + 1);
		}
	}
}
