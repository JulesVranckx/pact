package View;

import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.JPanel;

import Controller.Next;
import Controller.Previous;

public class PanelButonNextPrevious extends JPanel{
	private Next next;
	private Previous previous;

	public PanelButonNextPrevious(Window window) {
		// TODO Auto-generated constructor stub
		this.setBackground(Color.LIGHT_GRAY);
		this.setLayout(new FlowLayout());
		this.add(previous=new Previous(window));
		this.add(next=new Next(window));

	}
	

}

