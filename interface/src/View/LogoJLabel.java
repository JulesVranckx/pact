package View;

import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class LogoJLabel extends JLabel{

	public LogoJLabel() {
		super();
		String path = "../../img/logo.png";
		ImageIcon logo = new ImageIcon(new ImageIcon(path).getImage().getScaledInstance(75, 75, Image.SCALE_DEFAULT));;
		this.setIcon(logo);
	}
}
