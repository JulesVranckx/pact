package View;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import ModelAPP.Model;

import javax.imageio.ImageIO;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ImageJLabel extends JLabel {
	private final Window window;
	private String fileName;
	public ImageJLabel(Window window) {
		super();
		this.window=window;
		fileName=window.getModel().getFileName();
		ImageIcon image = new ImageIcon(fileName);
		int width = image.getIconWidth();
		int height = image.getIconHeight();
		double factorH = 150.0/height;
		double factorW = 110.0/width;
		double resizedWidth = factorW*width;
		double resizedHeight = factorH*height;
		
		image = new ImageIcon(new ImageIcon(fileName).getImage().getScaledInstance((int) resizedWidth,(int) resizedHeight, Image.SCALE_SMOOTH));
		this.setIcon(image);
		 	
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		fileName=window.getModel().getFileName();
  }
	public void notifyForUpdate(String arg1) {
		ImageIcon image = new ImageIcon(fileName);
		int width = image.getIconWidth();
		int height = image.getIconHeight();
		double factorH = 150.0/height;
		double factorW = 110.0/width;
		double resizedWidth = factorW*width;
		double resizedHeight = factorH*height;
		
		image = new ImageIcon(new ImageIcon(arg1).getImage().getScaledInstance((int) resizedWidth,(int) resizedHeight, Image.SCALE_SMOOTH));
		this.setIcon(image);
		repaint();
		
	}
	


}
