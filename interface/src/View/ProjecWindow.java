package View;

import java.awt.Dimension;
import java.awt.Point;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;

import ModelAPP.Model;

public class ProjecWindow extends JFrame implements Observer{
private ProjectionPanel draw;
private Model model;
   public ProjecWindow(Window window) {
	super("1,2,3 portrait");
	model=window.getModel();
	model.addObserver(this);
	this.setPreferredSize(new Dimension(700,700));
	this.setContentPane(draw=new ProjectionPanel(window));
	pack() ;   // sizes automatically the window    
    setVisible(true) ;
	this.setLocation(new Point(600,10));
   }
@Override
public void update(Observable o, Object arg) {
	// TODO Auto-generated method stub
	draw.notifyForUpdate();
}

public void notifyForUpdate() throws InterruptedException {
	draw.notifyForUpdate(); 
}

}
