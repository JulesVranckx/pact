package View;

import java.awt.Color;

import javax.swing.JLabel;

public class TextField extends JLabel{
	
	public TextField(Window window, int numStep) {
		super("Etape " + numStep);
		this.setBackground(Color.LIGHT_GRAY);
	}

}
