package View;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.image.ImageObserver;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.border.Border;

public class DrawingPanel extends JPanel {
	private ArrayList<ImageIcon> traitList;
	private final Window window;
	private int arg1;
	private int etape;
	private int step = 0;

	public final int getStep() {
		return step;
	}

	public final void setStep(int step) {
		this.step = step;
	}

	public DrawingPanel(Window window) {
		super();
		this.window = window;
		etape = window.getModel().getEtape();
		this.traitList = window.getModel().getTraitList().get(etape);
		this.setBackground(Color.WHITE);


	}

	public void notifyForUpdate() {
		repaint();
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		etape = window.getModel().getEtape();
		arg1 = window.getModel().getCount();
		this.traitList = window.getModel().getTraitList().get(etape);

		ImageIcon icon = new ImageIcon(traitList.get(arg1).getImage().getScaledInstance(this.getWidth(), this.getHeight(), Image.SCALE_DEFAULT));
		Image image = icon.getImage();
		g.drawImage(image, 0, 0, (ImageObserver) this);
		if ((etape > 0) && (etape == window.getModel().getTraitList().size() - 1) && (arg1 == window.getModel().getTraitList().get(etape).size() - 1)) {
			textEndMessage(step);
			this.setStep(1);
		}


	}

	public void textEndMessage(int j) {
		if (j == 0) {
			JOptionPane.showMessageDialog(null, "Bravo ! Le Portrait est " +
					"termine", "Information", JOptionPane.INFORMATION_MESSAGE);
		}
	}
}
