package View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Insets;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import Controller.FeedBackButton;
import Controller.Next;
import Controller.Previous;

public class PanelBar extends JPanel {
	private ImageJLabel imageJLabel;
	private LogoJLabel logo;
	private FeedBackButton feedback;
	private PanelButonNextPrevious NP;
	private TextField textField;
	private int etape;
	private final Window window;
	public ImageJLabel getImageJLabel() {
		return imageJLabel;
	}

	public void setImageJLabel(ImageJLabel imageJLabel) {
		this.imageJLabel = imageJLabel;
	}

	public PanelBar(Window window) {
		
		super();
		this.window=window;
		
		this.setBackground(Color.LIGHT_GRAY);
		this.setPreferredSize(new Dimension(200,200));
		this.setLayout(new GridBagLayout());
	
		//this.add(new JLabel("lefjsfjs"));
		GridBagConstraints c = new GridBagConstraints();
		
		textField = new TextField(window, etape);
		c.fill = GridBagConstraints.CENTER;
		//c.insets = new Insets(10,10,0,5);
		c.gridx = 2;
		c.gridy = 0;
		//this. add(textField,c);
		NP=new PanelButonNextPrevious(window);
		//JLabel etape=new JLabel("Etape" + NP.getPrevious().getPreviousCount()+"sur 31 :");
		 c.fill = GridBagConstraints.HORIZONTAL;
		   c.insets = new Insets(10,10,0,5);
		   c.gridx =2;
		   c.gridy = 0;
		  //this.add(etape,c);
		  
		   c.fill = GridBagConstraints.HORIZONTAL;
		   c.insets = new Insets(10,10,0,5);
		   c.gridx =2;
		   c.gridy =1;
		  this.add(NP,c);

		    imageJLabel = new ImageJLabel(window);
			c.fill = GridBagConstraints.CENTER;
			//c.weightx=0.0;
			c.insets = new Insets(10,10,10,5);
			c.gridy = 2;
			c.gridx = 2;
			this.add(imageJLabel, c);
			
			feedback=new FeedBackButton(window);
			c.fill = GridBagConstraints.HORIZONTAL;
			c.insets = new Insets(5,10,50,0);
			c.gridx = 2;
			c.gridy = 3;
			//c.ipady = 30;
			this.add(feedback,c);
			logo = new LogoJLabel();
			//c.fill = GridBagConstraints.HORIZONTAL;
			c.insets = new Insets(90,50,0,10);  
			c.gridx =2	;       //aligned with button 2
			c.gridy = 4;       //third row
			this.add(logo, c);
			
		
		
		
	}
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		etape=window.getModel().getEtape();
		textField = new TextField(window, etape);
		g.drawString("Etape :"+etape, 80,90);
		g.setColor(Color.LIGHT_GRAY);
		
	}
	public void notifyForUpdate(String arg1) {
		// TODO Auto-generated method stub
		this.imageJLabel.notifyForUpdate((String) arg1);
	}

	public final PanelButonNextPrevious getNP() {
		return NP;
	}

	public final void setNP(PanelButonNextPrevious nP) {
		NP = nP;
	}

	public void notifyForUpdate() {
		// TODO Auto-generated method stub
		repaint();

	}

	
}
