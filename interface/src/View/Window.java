package View;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;

import JMenu.JmenuBar123Portrait;
import ModelAPP.Model;

//import ModelAPP.ModelWindow;

public class Window extends JFrame implements Observer{
	private JmenuBar123Portrait bar;
	private Model model;
	private ProjecWindow projec;
	public WindowPanel getWindowpanel() {
		return windowpanel;
	}
	public Model getModel() {
		return model;
	}

	public void setModel(Model model) {
		this.model = model;
	}
	public void setWindowpanel(WindowPanel windowpanel) {
		this.windowpanel = windowpanel;
	}
	private WindowPanel windowpanel;
	public Window() {
		super("1,2,3 portrait");
	  
		
		this.setPreferredSize(new Dimension(700,700));
		 
  	
		model=new Model();
		model.addObserver(this);
		setContentPane(windowpanel=new WindowPanel(this));
		setJMenuBar(bar=new JmenuBar123Portrait(this));
		this.setBackground(Color.PINK);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE) ; 
	    pack() ;   // sizes automatically the window    
	    setVisible(true) ;
	    
	    projec=new ProjecWindow(this);
	}
	

	@Override
	public void update(Observable arg0, Object arg1) {
		// TODO Auto-generated method stub	
			if( (arg1!=null)) {
				windowpanel.notifyForUpdate((String) arg1);
			}
			else {
				try {
					windowpanel.notifyForUpdate();
					projec.notifyForUpdate();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}	
			}
				
		
				
	}
	}

//}
