package View;

import java.awt.BorderLayout;
import java.util.ArrayList;

import javax.swing.JPanel;

public class WindowPanel extends JPanel{
	private DrawingPanel drawpanel;
	private PanelBar panelbar;
	public WindowPanel(Window window) {
		super();
		
		setLayout(new BorderLayout());
		this.add(drawpanel=new DrawingPanel(window), BorderLayout.CENTER);
		this.add(panelbar=new PanelBar(window), BorderLayout.EAST);		
	
		
	}
	public PanelBar getPanelbar() {
		return panelbar;
	}
	public void setPanelbar(PanelBar panelbar) {
		this.panelbar = panelbar;
	}
	public void notifyForUpdate(String arg1) {
		panelbar.notifyForUpdate((String) arg1);
		
	}
	public void notifyForUpdate() throws InterruptedException {
		drawpanel.notifyForUpdate(); 
		panelbar.notifyForUpdate();
	}

}
