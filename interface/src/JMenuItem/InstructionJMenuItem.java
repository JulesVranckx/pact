package JMenuItem;

import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

public class InstructionJMenuItem extends JMenuItem implements ActionListener{
	private final Window window;

	public InstructionJMenuItem(Window window) {
		super("Instruction");
		
		this.window = window;
		this.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
JFrame aboutWindow = new JFrame("Instruction");
		
		aboutWindow.setLocationRelativeTo(null);
		aboutWindow.setPreferredSize(new Dimension(500,500));
		JPanel aboutWindowPanel = new JPanel();
		aboutWindowPanel
				.add(new JLabel(
						"<HTML>*****Welcome 1,2,3 portrait****<br>"
								  + "<br>Enjoy!!<HTML>"));
								
																		

		aboutWindow.setContentPane(aboutWindowPanel);
		aboutWindow.pack();
		aboutWindow.setVisible(true);

		
	}
	

}
