package JMenuItem;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import javax.swing.filechooser.FileNameExtensionFilter;

import View.Window;

public class SaveAs extends JMenuItem implements ActionListener{
	private final Window window;
	public SaveAs(Window window) {
		super("Save As...");
		setIcon(new ImageIcon("icon/save.png"));
		setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK));
		this.window= window;
		this.addActionListener(this);
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		JFileChooser fc = new JFileChooser();
		fc.removeChoosableFileFilter(fc.getAcceptAllFileFilter());
		FileNameExtensionFilter filter = new FileNameExtensionFilter("tous",".JPG"); 
		fc.setFileFilter(filter);
		int returnVal = fc.showSaveDialog(getParent());

		if (returnVal == JFileChooser.APPROVE_OPTION) {
			String path = fc.getSelectedFile().getAbsolutePath();
			window.getModel().saveAs(path);
				
			}
		}
		
	}


