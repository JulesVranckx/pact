package JMenuItem;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import View.Window;

public class OpenFile extends JMenuItem implements ActionListener{
	private final Window window;
	public OpenFile(Window window) {
		super("OpenFile");
		this.window=window;
		this.addActionListener(this);
		setIcon(new ImageIcon("icon/open.png"));
		setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_DOWN_MASK));
		

	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
		JFileChooser dialog = new JFileChooser("../../img");
		dialog.showOpenDialog(null);
		if (dialog.getSelectedFile() != null) {
			 String tmp=window.getModel().getTp();
			 File dir = new File(tmp);
			 int i=0;

			// set up the command and parameter
	         String pythonScriptPath = "../../process/main.py";
	          String[] cmd = new String[4];
	          cmd[0] = "python3";
	          cmd[1] = pythonScriptPath;
	          cmd[2]=dialog.getSelectedFile().getAbsoluteFile().getAbsolutePath();
	          cmd[3]="tmp";
	           // create runtime to execute external command
	          Runtime rt = Runtime.getRuntime();
	          Process pr = null;
	          try {
				 pr = rt.exec(cmd);
			} catch (IOException e) {
				e.printStackTrace();
			}
	         // retrieve output from python script
				
				  BufferedReader bfr = new BufferedReader(new InputStreamReader(pr.getInputStream())); 
				  String line = ""; 
				  try {
					while((line = bfr.readLine()) != null) { // display each output line form python script
					  System.out.println(line); }
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				 
	         
	         if(!isDirectoryEmpty(dir))
	         {
	        	 window.getModel().setCount(0);
				 window.getModel().setEtape(0);
				  window.getModel().createList(dialog.getSelectedFile().getAbsoluteFile().getAbsolutePath(),tmp); 
	         }else {
				 BufferedReader stdError = new BufferedReader(new
						 InputStreamReader(pr.getErrorStream()));
				 System.out.println("The python script encountered an " +
						 "error:\n");
				 String s;
				 while (true) {
					 try {
						 if ((s = stdError.readLine()) == null) break;
					 } catch (IOException e) {
						 e.printStackTrace();
						 break;
					 }
					 System.out.println(s);
				 }


			 }
	         
	      
		
		  }	
	}
	public static boolean isDirectoryEmpty(File file){
		  if(file != null && file.isDirectory()){
		   return (file.list().length == 0);
		  }
		  return false;
		 }
		
	
}
