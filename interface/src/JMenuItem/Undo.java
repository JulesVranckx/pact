package JMenuItem;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JMenuItem;

import View.Window;

public class Undo extends JMenuItem implements ActionListener{
	private final Window window;
	public Undo(Window window) {
		super("Undo");
		this.window=window;
		this.addActionListener(this);
		setIcon(new ImageIcon("icon/undo.png"));
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		int k = window.getModel().getCount();
		int etape = window.getModel().getEtape();
		window.getModel().setIsmodified(false);

		if (k >= 2) {
			window.getModel().setCount(k - 2);
		} else {
			if (k == 0 && etape > 0) {
				window.getModel().setEtape(etape - 1);
				window.getModel().setCount(window.getModel().getTraitList().get(etape - 1).size() - 1);
			}
			if (k == 1) {
				window.getModel().setCount(0);
			}
		}
	}
}
