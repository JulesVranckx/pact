package JMenuItem;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import View.Window;

public class Exit extends JMenuItem implements ActionListener{
	private final Window window;
	public Exit(Window window) {
		super("Exit");
		this.window=window;
		setIcon(new ImageIcon("icon/exit.png"));
		this.addActionListener(this);
	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
		int response = JOptionPane.showConfirmDialog(this, "Really quit ?", "Quit application",
				JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE);
		switch (response) {
		case JOptionPane.OK_OPTION:
			System.exit(0);
		case JOptionPane.NO_OPTION:
			break;
		}
		
	}

}
