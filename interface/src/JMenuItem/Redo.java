package JMenuItem;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JMenuItem;

import View.Window;

public class Redo extends JMenuItem implements ActionListener{
	private final Window window;
	public Redo(Window window) {
		super("Redo");
		this.window = window;
		this.addActionListener(this);
		setIcon(new ImageIcon("icon/redou.png"));
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		int k = window.getModel().getCount();
		int etape=window.getModel().getEtape();
		window.getModel().setIsmodified(false);


		if ((k< window.getModel().getTraitList().get(etape).size()-2))
		{
			window.getModel().setCount(k+2);
		}else {
			if(k==window.getModel().getTraitList().get(etape).size()-1&&etape<window.getModel().getTraitList().size()-1) {
				window.getModel().setEtape(etape+1);
				window.getModel().setCount(0);
			}
			if(k==window.getModel().getTraitList().get(etape).size()-2) {

				window.getModel().setCount(k+1);
			}
		}


	}
}
