package Controller;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import View.Window;

public class Previous extends JButton implements ActionListener{
	private final Window window;
	
	public Previous (Window window) {
		super("<<");
		this.window=window;
		setBackground(Color.WHITE);
		setBorderPainted(false);
		setOpaque(true);
		this.addActionListener(this);
	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
		int k = window.getModel().getCount();	
		int etape=window.getModel().getEtape();
		window.getModel().setIsmodified(false);
		
		if (k >=2)
		{
			window.getModel().setCount(k-2);
		}else {
			if(k==0&&etape>0)
			{
				window.getModel().setEtape(etape-1);
				window.getModel().setCount(window.getModel().getTraitList().get(etape-1).size()-1);
			}
			if(k==1) {
				window.getModel().setCount(0);
			}
		}
	}
		
		/*int k = window.getModel().getCount();	
		k = k-2;
		if(window.getModel().getTraitList().size()>1)
		window.getModel().setCount(k);
		
		}*/
		
	}
	


