package Controller;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import View.Window;

public class Next extends JButton implements ActionListener {
	private final Window window;

	public Next(Window window) {
		super(">>");
		this.window = window;
		setBackground(Color.WHITE);
		setBorderPainted(false);
		setOpaque(true);
		this.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		int k = window.getModel().getCount();
		int etape = window.getModel().getEtape();
		window.getModel().setIsmodified(false);


		if ((k < window.getModel().getTraitList().get(etape).size() - 2)) {
			window.getModel().setCount(k + 2);
		} else {
			if (k == window.getModel().getTraitList().get(etape).size() - 1 && etape < window.getModel().getTraitList().size() - 1) {
				window.getModel().setEtape(etape + 1);
				window.getModel().setCount(0);
			}
			if (k == window.getModel().getTraitList().get(etape).size() - 2) {

				window.getModel().setCount(k + 1);
			}
		}

	}
}
