package Controller;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import View.FeedBackWindow;
//import View.FeedBackWindow;
import View.Window;

public class FeedBackButton extends JButton implements ActionListener{
	private final Window window;

	
	public FeedBackButton(Window window) {
		super("Feedback");
		this.window = window;
		this.addActionListener(this);
		this.setBackground(Color.WHITE);
		setBorderPainted(false);
		setOpaque(true);
		//this.setPreferredSize(new Dimension(10,10));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		JFileChooser dialog = new JFileChooser("../../img");
		window.getModel().setIsmodified(true);
		dialog.showOpenDialog(null);

		if (dialog.getSelectedFile() != null) { 
			 String pythonScriptPath = "../../feedback/main.py";
	          String[] cmd = new String[5];
	          cmd[0] = "python3";
	          cmd[1] = pythonScriptPath;
	          cmd[2]=dialog.getSelectedFile().getAbsoluteFile().getAbsolutePath();
	          cmd[3]=window.getModel().getFileName();
	          cmd[4]="../../feedback/datas.json";
	           // create runtime to execute external command
	          Runtime rt = Runtime.getRuntime();
	          Process pr = null;
	          try {
				 pr = rt.exec(cmd);
				  BufferedReader stdError = new BufferedReader(new
						  InputStreamReader(pr.getErrorStream()));
				  String s;
				  while (true) {
					  try {
						  if ((s = stdError.readLine()) == null) break;
					  } catch (IOException e1) {
						  e1.printStackTrace();
						  break;
					  }
					  System.out.println(s);
				  }
			  } catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	         // retrieve output from python script
				
				  BufferedReader bfr = new BufferedReader(new InputStreamReader(pr.getInputStream())); 
				  String line = ""; 
				  try {
					while((line = bfr.readLine()) != null) { // display each output line form python script
					  System.out.println(line); }
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				  

		new FeedBackWindow(window,"../../feedback/datas.json");

		}
		}	
	}

	

