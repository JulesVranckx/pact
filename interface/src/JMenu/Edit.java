package JMenu;
import javax.swing.JMenu;

import JMenuItem.Redo;
import JMenuItem.Undo;
import View.Window;

public class Edit extends JMenu{
	private Undo undo;
	private Redo redo;
	public Edit(Window window) {
		super("Edit");
		this.add(undo=new Undo(window));
		this.add(redo=new Redo(window));
		
	}
	

}
