package JMenu;
import javax.swing.ImageIcon;
import javax.swing.JMenu;

import JMenuItem.Exit;
import JMenuItem.OpenFile;

import JMenuItem.SaveAs;
import View.Window;

public class File extends JMenu{
	private OpenFile openfile;
	private Exit exit;
	
	private SaveAs saveAS;
	public File( Window window) {
		super("File");
		
		this.add(openfile=new OpenFile(window));
		
		this.add(saveAS=new SaveAs(window));
		this.add(exit = new Exit(window));
	}
 
}
