package ModelAPP;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Observable;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;


public class Model extends Observable {
	private String fileName;
	private ArrayList<ArrayList<ImageIcon>> traitList;
	private int count = 0;
	private int etape = 0;
	private boolean ismodified=false;
	String tp="../../process/tmp";
	public boolean isIsmodified() {
		return ismodified;
	}

	public void setIsmodified(boolean ismodified) {
		this.ismodified = ismodified;
		this.setChanged();
		this.notifyObservers();
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
	
		//createList();
		this.setChanged();
		this.notifyObservers(fileName);
		
		
	}

	public Model() {
		this.fileName = "../../img/first.png";
		traitList=new ArrayList<ArrayList<ImageIcon>>();
		ArrayList<ImageIcon> tmp=new ArrayList<ImageIcon>();
		tmp.add(new ImageIcon(fileName));
		traitList.add(tmp);
	}

	public int getEtape() {
		return etape;
	}

	public void setEtape(int etape) {
		this.etape = etape;
		this.setChanged();
		this.notifyObservers();
	}

	public final ArrayList<ArrayList<ImageIcon>> getTraitList() {
		return traitList;
	}

	public final void setTraitList(ArrayList<ArrayList<ImageIcon>> traitList) {
		this.traitList = traitList;
		
	}

	public void saveAs(String path) {
		Image image = traitList.get(etape).get(count).getImage();
		BufferedImage bf = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_RGB);
		bf.getGraphics().drawImage(image, 0, 0, null);
		File file = new File(path + ".png");

		try {
			ImageIO.write(bf, "png", file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public final int getCount() {
		return count;
	}

	public final void setCount(int count) {
		
			this.count = count;
			
			this.setChanged();
			this.notifyObservers();
		
			
	}
	
	public final void changeCount(int count) {
		this.count = count;
	}
	public void createList(String string,String tp) {
		this.tp=tp;
		int k=0;
		int i=0;
		
		ArrayList<ArrayList<ImageIcon>> tmp=new ArrayList<ArrayList<ImageIcon>>();
		ArrayList<ImageIcon> list=new ArrayList<ImageIcon>();
	
		list.add(new ImageIcon(fileName));
		String name=k+"_"+i+".png";
		File file =new File(tp+"/"+name);
		
		while(file.exists()) {
			while(file.exists()){
				list.add(new ImageIcon(tp+"/"+name));
				 name=k+"_"+i+".png";
				 file =new File(tp+"/"+name);
				i++;
			}
			i=0;
		    k++;
		    
		    name=k+"_"+i+".png";
		    file =new File(tp+"/"+name);
		
		    	  tmp.add(list);
				  list=new ArrayList<ImageIcon>();
		  
		  
		   
		    
		}
		this.setFileName(string);  
		this.traitList=tmp;
		tmp=null;
		list=null;
	
		  
		  File dir = new File ("tmp"); dir.delete();
		 
	}

	public String getTp() {
		return tp;
	}

	public void setTp(String tp) {
		this.tp = tp;
	}
	

}
