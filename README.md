# 1, 2, 3 Portrait
Dessiner des portraits n'a jamais été aussi facile !  
<img src="img/logo.png" width=200 height=200>

## Instalation
Unix uniquement.
Assurez vous d'avoir `pip` et `python 3.7` sur votre ordinateur. Installez les librairies python nécessaires avec:  
```pip -r requirements.txt```  
Compilez les éxecutables avec:  
```make```  
Et lancez l'application avec:  
```./123portrait```

